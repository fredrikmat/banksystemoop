package fm.project.bank;

/**
 *
 * @author Fredrik
 */
public class BankAccountType {
    private int type;
    private double rent;
    
    public BankAccountType( int type ) {
        
        this.type = type;
        this.rent = type == 0 ? 0.00 : 3.59;
    }

    public int getType() { return type; }

    public void setType(int type) { this.type = type; }

    public double getRent() { return rent; }

    public void setRent(double rent) { this.rent = rent; }
}
