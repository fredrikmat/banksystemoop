-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 17, 2016 at 10:37 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fmbanken`
--

-- --------------------------------------------------------

--
-- Table structure for table `core_accountowners`
--

CREATE TABLE `core_accountowners` (
  `bankaccount` bigint(12) NOT NULL,
  `ssn` bigint(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `core_bankaccounts`
--

CREATE TABLE `core_bankaccounts` (
  `id` bigint(12) NOT NULL,
  `balance` float(10,2) DEFAULT NULL,
  `type` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `core_transactionhistory`
--

CREATE TABLE `core_transactionhistory` (
  `id` int(255) NOT NULL,
  `date` datetime NOT NULL,
  `amount` float(10,2) NOT NULL,
  `source` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `core_transactionhistoryowner`
--

CREATE TABLE `core_transactionhistoryowner` (
  `historyId` int(255) NOT NULL,
  `accountId` bigint(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `core_users`
--

CREATE TABLE `core_users` (
  `ssn` bigint(12) NOT NULL,
  `name` varchar(100) NOT NULL,
  `permission` int(11) NOT NULL DEFAULT '1',
  `pass_hash` varchar(255) NOT NULL,
  `pin_hash` varchar(255) NOT NULL,
  `salt` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `core_accountowners`
--
ALTER TABLE `core_accountowners`
  ADD PRIMARY KEY (`bankaccount`),
  ADD KEY `FK_core_accountowners_ssn` (`ssn`);

--
-- Indexes for table `core_bankaccounts`
--
ALTER TABLE `core_bankaccounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `core_transactionhistory`
--
ALTER TABLE `core_transactionhistory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `core_transactionhistoryowner`
--
ALTER TABLE `core_transactionhistoryowner`
  ADD KEY `accountId` (`accountId`),
  ADD KEY `core_transactionhistoryowner_ibfk_1` (`historyId`);

--
-- Indexes for table `core_users`
--
ALTER TABLE `core_users`
  ADD PRIMARY KEY (`ssn`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `core_transactionhistory`
--
ALTER TABLE `core_transactionhistory`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `core_accountowners`
--
ALTER TABLE `core_accountowners`
  ADD CONSTRAINT `FK_core_accountowners_bankaccount` FOREIGN KEY (`bankaccount`) REFERENCES `core_bankaccounts` (`id`),
  ADD CONSTRAINT `FK_core_accountowners_ssn` FOREIGN KEY (`ssn`) REFERENCES `core_users` (`ssn`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
