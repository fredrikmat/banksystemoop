package fm.project.repositories;

import fm.project.bank.BankAccount;
import fm.project.bank.PaymentException;
import fm.project.bank.User;
import fm.project.util.Util;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Locale;

/**
 *
 * @author Fredrik
 */
public class BankAccountRepository {
    
    public BankAccount find( String predicate ) throws PaymentException, SQLException {

        Util.connectToDb();
        Statement stmt = Util.getDbConnection().createStatement( );

        ResultSet rs = stmt.executeQuery( String.format("SELECT * FROM core_bankaccounts WHERE id = '%s'", predicate.replaceAll("\\s+","")) );

        if(rs.next() != false) {
            return new BankAccount( rs.getString("id"), rs.getDouble("balance"), rs.getInt("type") );   
        }
        else {
            throw new PaymentException("No bank account was found");
        }
    }
    
    public void save( BankAccount account, User user ) throws SQLException {
        Util.connectToDb();
        Statement stmt = Util.getDbConnection().createStatement( );

        stmt.executeUpdate( String.format(Locale.US, "INSERT INTO core_bankaccounts values('%s', '%.2f', '%d')", account.getId(), account.getBalance(), account.getType().getType() ) );
        stmt.executeUpdate( String.format( "INSERT INTO core_accountowners values('%s', '%d')", account.getId(), user.getSsn() ) );
    }
    
    public ArrayList<String> getHistory( BankAccount account ) throws SQLException {
        Util.connectToDb();
        Statement stmt = Util.getDbConnection().createStatement( );

        ResultSet rs = stmt.executeQuery( String.format("SELECT core_transactionhistory.* FROM core_transactionhistory INNER JOIN core_transactionhistoryowner ON core_transactionhistory.id=core_transactionhistoryowner.historyId AND core_transactionhistoryowner.accountId = %s", account.getId() ) );

        ArrayList<String> history = new ArrayList<>();
        
        while( rs.next() ) {
            history.add( String.format( Locale.US, "%s\n%.2f SEK - %s\n", rs.getString("date"), rs.getDouble("amount"), rs.getString("source") ) );
        }
        
        return history;
    }
    
}
