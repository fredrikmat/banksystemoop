package fm.project.repositories;

import fm.project.bank.BankAccount;
import fm.project.bank.Payment;
import fm.project.util.Util;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Locale;


/**
 *
 * @author Fredrik
 */
public class HistoryRepository {
    
    public void save( BankAccount account, Payment payment) throws SQLException {

        Util.connectToDb();
        Statement stmt = Util.getDbConnection().createStatement( );
        
        java.util.Date dt = new java.util.Date();
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentTime = sdf.format(dt);
        
        String source = (payment.getOperator() == +1) ? payment.getPayer().getId() : payment.getReceiver().getId();
        
        stmt.executeUpdate( String.format(Locale.US, "INSERT INTO core_transactionhistory (date, amount, source) values('%s', '%.2f', '%s')", currentTime, payment.getAmount() * payment.getOperator(), source) );
        stmt.executeUpdate( String.format( "INSERT INTO core_transactionhistoryowner values(LAST_INSERT_ID(), '%s')", account.getId() ) );
    }
}
