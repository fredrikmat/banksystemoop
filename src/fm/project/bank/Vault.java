package fm.project.bank;

/**
 *
 * @author Fredrik
 */
public class Vault {
    
    private double money;
    
    public Vault() {
        this.money = 1000000; // Läs ifrån XML / Databas sedan
    }
    
    public double getMoney() { return money; }

    public void setMoney(double money) { this.money += money; }
    
}
