package fm.project.util;

import java.sql.*;

import java.util.Random;

/**
 *
 * @author Fredrik
 */
public class Util {
    
    private static Connection connection;
    
    public static int randInt(int min, int max) {
        Random rand = new Random();
        
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }
    
    /*
        Database related functions
    */
    public static void connectToDb(){
        try{
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/fmbanken", "root", "");
        } catch(ClassNotFoundException | SQLException e){
            System.out.println(e);
        }
    }
    
    public static boolean isDbConnected() throws SQLException{
        return ! connection.isClosed();
    }
    
    public static Connection getDbConnection() {
        return connection;
    }
    
}
