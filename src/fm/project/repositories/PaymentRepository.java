package fm.project.repositories;

import fm.project.bank.BankAccount;
import fm.project.bank.Payment;
import fm.project.util.Util;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Locale;

/**
 *
 * @author Fredrik
 */
public class PaymentRepository extends HistoryRepository {
    
    @Override
    public void save( BankAccount account, Payment payment ) throws SQLException {
        
        Util.connectToDb();
        Statement stmt = Util.getDbConnection().createStatement( );
        stmt.executeUpdate( String.format(Locale.US, "UPDATE core_bankaccounts SET balance = '%.2f' WHERE id = %s", account.getBalance(), account.getId() ) );

        super.save(account, payment);
        
    }
}
