package fm.project.bank;

import fm.project.util.Util;
import java.sql.*;
import java.util.ArrayList;

/**
 *
 * @author Fredrik
 */
public class User {
    
    protected String name;
    protected long ssn; // Social Security Number
    protected int permission;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getSsn() {
        return ssn;
    }
    
    public void SetSsn(long ssn) {
        this.ssn = ssn;
    }

    public int getPermission() {
        return permission;
    }

    public void setPermission(int permission) {
        this.permission = permission;
    }
    
    public boolean login( String ssn, String pin ) {
        try {
            
            Util.connectToDb();
            Statement stmt = Util.getDbConnection().createStatement( );

            ResultSet rs = stmt.executeQuery( String.format("SELECT * FROM core_users WHERE ssn = '%s' AND pin_hash = '%s'", ssn, pin) );

            if(rs.next() != false) {
                
                this.ssn = rs.getLong("ssn");
                this.name = rs.getString("name");
                this.permission = rs.getInt("permission");
                
                return true;
            } 
        }
        catch(Exception e) {
            System.out.println( e );
        }
        
        return false;
    }
    
    public ArrayList<BankAccount> getBankAccounts() {
        
        ArrayList<BankAccount> accounts = new ArrayList<>();
        
        try {
            
            Util.connectToDb();
            Statement stmt = Util.getDbConnection().createStatement( );

            ResultSet rs = stmt.executeQuery( String.format("SELECT core_bankaccounts.* FROM core_bankaccounts INNER JOIN core_accountowners ON core_bankaccounts.id=core_accountowners.bankaccount AND core_accountowners.ssn = %s ORDER BY type DESC", this.ssn ) );

            while(rs.next()) {
                accounts.add( new BankAccount( rs.getString("id"), rs.getDouble("balance"), rs.getInt("type") ) );
            } 
        }
        catch(Exception e) {
            System.out.println( e );
        }
        
        return accounts;
    }
}
