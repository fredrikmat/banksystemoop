package fm.project.bank;

/**
 *
 * @author Fredrik
 */
public class PaymentException extends Exception{
    public PaymentException(String message) {
        super(message);
    }
}
