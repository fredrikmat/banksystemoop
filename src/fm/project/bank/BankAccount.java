package fm.project.bank;

import fm.project.repositories.BankAccountRepository;
import fm.project.util.Util;
import java.util.ArrayList;

/**
 *
 * @author Fredrik
 */
public class BankAccount {
    
    private String id;
    private double balance;
    private BankAccountType type;
    private BankAccountRepository bankAccountRepository;
    
    
    public BankAccount() {
        this.id = "";
        for(int i = 0; i < 12; i++) {
            this.id += Util.randInt(0, 9);
        }
        this.balance = 0.00;
        this.bankAccountRepository = new BankAccountRepository();
    }
    
    public BankAccount(String id, double balance, int type) {
        this.id = id;
        this.balance = balance;
        this.type = new BankAccountType(type);
        this.bankAccountRepository = new BankAccountRepository();
    }
    
    public void save( User user ) {
        
        try {
            this.bankAccountRepository.save(this, user);

        } catch( Exception e ) { System.out.println(e); }
    }
    
    public String getFormatedId() {
        String formatedId = "";
        for(int i = 1; i < 13; i++) {
            formatedId += this.id.substring(i-1, i);
            if(i % 4 == 0) formatedId += " ";
        }
        return formatedId;
    }
    
    public ArrayList<String> getFormatedHistory() {
        try {
            return this.bankAccountRepository.getHistory(this);
        } catch( Exception e ) { System.out.println(e); }
        
        return null;
    }
    
    public String getId() { return id; }
    
    public double getBalance() { return balance; }

    public void setBalance(double balance) { this.balance = balance; }
    
    public BankAccountType getType() { return type; }

    public void setType(int type) { this.type = new BankAccountType(type); }
    
    @Override
    public String toString() {
        return this.id;
    }  
}
